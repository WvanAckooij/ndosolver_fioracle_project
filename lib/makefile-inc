##############################################################################
############################### makefile-inc #################################
##############################################################################
#                                                                            #
#   makefile of libNDO as a pre-built library                                #
#                                                                            #
#   This makefile is thought to be included by the makefile of some code     #
#   using the libNDO library. However, this makefile treats libNDO as a      #
#   pre-built library, in the sense that it ensures that it is built (by a   #
#   separate make process) and then returns as $(libNDOOBJ) the complete     #
#   libNDO library together with macros $(libNDOINC) and $(libNDOLIB) for    #
#   includes and external libreries. It is therefore a way to keep the make  #
#   process of libNDO separate from the make process of the code using it,   #
#   as opposed to directly including makefile-c that "entirely merges the    #
#   two make processes°. However, note that if $(CC) and $(SW) are defined   #
#   by the including makefile, they will be "forwarded" to the makefile of   #
#   libNDO so as to ensure that the compiler/options are consistent between  #
#   the two make processes.                                                  #
#                                                                            #
#   Input:  $(libNDOSDR) = the directory where the libNDO source is          #
#                                                                            #
#           optional: $(CC) = the C++ compiler command                       #
#                                                                            #
#           optional: $(SW) = the compiler options (optimization, debug,     #
#                     required C++ standard, ...)                            #
#                                                                            #
#   Output: there is no $(libNDOH) output, since the calling makefile has    #
#           no need to check for changes in the .h and rebuild the .a: this  #
#           is all done here inside. However, the following macros are       #
#           provided because the code using libNDO need to know where to     #
#           find the .h files to include, which external libreries and       #
#            -L<libdirs> need be used in the linking phase, and of course    #
#           libNDO.a need be linked against.                                 #
#           $(libNDOINC) = the -I$(include directories) for libNDO           #
#           $(libNDOLIB) = external libreries + -L<libdirs> for libNDO       #
#           $(libNDOOBJ) = the libNDO.a library itself                       #
#                                                                            #
#   Internally, the makefile calls makefile-c, strips away the *H macro and  #
#   redefines the *OBJ one. It then uses the standard front-end makefile-lib #
#   to have the library built, "forwarding" it $(CC) and $(SW) if they have  #
#   been provided by the including makefile.                                 #
#                                                                            #
#                              Antonio Frangioni                             #
#                         Dipartimento di Informatica                        #
#                             Universita' di Pisa                            #
#									     #
##############################################################################

# include the libNDO makefile internally defining all external modules - - - -

include $(libNDOSDR)lib/makefile-c

# re-define the OBJ as the library- - - - - - - - - - - - - - - - - - - - - -

libNDOOBJ = $(libNDOSDR)lib/libNDO.a

# ensure that the library is properly constructed - - - - - - - - - - - - - -
# force the make to be always executed

$(libNDOOBJ): FORCE
ifndef CC
ifndef SW
	make -f $(libNDOSDR)lib/makefile-lib libNDOSDR=$(libNDOSDR)
else
	make -f $(libNDOSDR)lib/makefile-lib libNDOSDR=$(libNDOSDR) SW='$(SW)'
endif
else
ifndef SW
	make -f $(libNDOSDR)lib/makefile-lib libNDOSDR=$(libNDOSDR) CC=$(CC)
else
	make -f $(libNDOSDR)lib/makefile-lib libNDOSDR=$(libNDOSDR) \
	CC=$(CC) SW='$(SW)'
endif
endif
FORCE:

# clean-up unrequired things- - - - - - - - - - - - - - - - - - - - - - - - -

libNDOH =

# clean - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

clean::
	make -f $(libNDOSDR)lib/makefile-lib clean libNDOSDR=$(libNDOSDR) 

########################## End of makefile-inc ###############################
